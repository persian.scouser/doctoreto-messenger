<?php

namespace Doctoreto\Messenger\Channels;

use GuzzleHttp\Client;
use Illuminate\Notifications\Notification;

class VerificationSmsChannel
{
    /**
     * The Guzzle client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * The phone number notifications should be sent from.
     *
     * @var string
     */
    protected $from;

    /**
     * Create a new Sms channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        $kavenegarConfig = config('services.kavenegar');

        if (is_null($kavenegarConfig)) {
            throw new \InvalidArgumentException('In order to send sms via kavenegar you need to add credentials in the `kavenegar` key of `config.services`.');
        }

        $this->client = new Client([
            'base_uri' => "https://api.kavenegar.com/v1/{$kavenegarConfig['api_key']}/verify/",
        ]);
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return \GuzzleHttp\Psr7\Response
     */
    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor('sms')) {
            return;
        }

        $message = $notification->toSms($notifiable);
        
        return $this->client->get(
            'lookup.json?'.
            "receptor=" . $to .
            "&token=" . $message->token .
            "&template=" . $message->template

        );
    }
}
