<?php

namespace Doctoreto\Messenger\Channels;

use GuzzleHttp\Client;
use Berkayk\OneSignal\OneSignalClient;
use Illuminate\Notifications\Notification;

class OneSignalChannel
{
    /** @var OneSignalClient */
    protected $oneSignal;

    /**
     * Create a new Sms channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        $oneSignalConfig = config('services.onesignal');

        if (is_null($oneSignalConfig)) {
            throw new \InvalidArgumentException('In order to send notification via OneSignal you need to add credentials in the `onesignal` key of `config.services`.');
        }

        $this->oneSignal = new OneSignalClient(
            $oneSignalConfig['app_id'],
            $oneSignalConfig['rest_api_key'],
            ''
        );
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return \GuzzleHttp\Psr7\Response
     */
    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor('OneSignal')) {
            return;
        }

        $message = $notification->toOneSignal($notifiable);

        $payload = $this->makePayload($to, $message);
        
        $response = $this->oneSignal->sendNotificationCustom($payload);

        return $response;
    }

    public function makePayload(array $segments, $message)
    {
        $payload = [
            'contents' => ['en' => $message->content],
            'headings' => ['en' => $message->header],
            'url' => $message->url,
            'included_segments' => $segments,
        ];
        
        $payload = array_filter($payload);

        return $payload;
    }
}
