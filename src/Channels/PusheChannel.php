<?php

namespace Doctoreto\Messenger\Channels;

use GuzzleHttp\Client;
use Illuminate\Notifications\Notification;

class PusheChannel
{
    /**
     * The Guzzle client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $client;

    private $url = 'https://panel.pushe.co/api/v1/notifications';

    private $pusheToken;

    /**
     * Create a new Pushe channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        $pusheConfig = config('services.pushe');

        if (is_null($pusheConfig)) {
            throw new \InvalidArgumentException('In order to send notification via pushe you need to add credentials in the `pushe` token of `config.services`.');
        }

        $this->pusheToken = $pusheConfig['api_token'];

        $this->client = new Client();
    }

    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return \GuzzleHttp\Psr7\Response
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$to = $notifiable->routeNotificationFor('pushe')) {
            $pusheId = [];
        }else{
            $pusheId = [$to];
        }

        if (!$notification->all && empty($pusheId)) {
            return;
        }

        if (app()->environment() !== 'production') {

            $pusheId = ["pid_47a5-d079-32"];
        }
        $content = $notification->toPushe($notifiable);

        $json = [
            "applications" => ["ir.hivatec.doctoreto"],
            "notification" => [
                "show_app" => false
            ],
            "custom_content" => $content
        ];

        if(!empty($pusheId)){
            $json['filter']['pushe_id'] = $pusheId;
        }

        return $this->client->request('POST',$this->url, [
            'json' => $json,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Token ' . $this->pusheToken
            ]
        ]);
    }
}
