<?php

namespace Doctoreto\Messenger\Messages;

class OneSignalMessage
{
    /** @var string */
    public $content;

    /** @var string */
    public $header;

    /** @var string */
    public $url;
    
    /**
     * @param string $body
     */
    public function __construct($content = '')
    {
        $this->content = $content;
    }

    /**
     * Set the message content.
     *
     * @param string $value
     *
     * @return $this
     */
    public function content($value)
    {
        $this->content = $value;

        return $this;
    }

    /**
     * Set the message header.
     *
     * @param string $value
     *
     * @return $this
     */
    public function header($value)
    {
        $this->header = $value;

        return $this;
    }

    /**
     * Set the message url.
     *
     * @param string $value
     *
     * @return $this
     */
    public function url($value)
    {
        $this->url = $value;

        return $this;
    }
}
