<?php

namespace Doctoreto\Messenger\Messages;

class VerificationSmsMessage
{
    /**
     * The message token.
     *
     * @var string
     */
    public $token;

    /**
     * The message template.
     *
     * @var string
     */
    public $template;


    public function __construct()
    {

    }

    /**
     * Set the message token.
     *
     * @param  string  $token
     * @return $this
     */
    public function token($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Set the message template.
     *
     * @param  string  $template
     * @return $this
     */
    public function template($template)
    {
        $this->template = $template;

        return $this;
    }


}
