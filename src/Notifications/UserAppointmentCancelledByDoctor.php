<?php

namespace Doctoreto\Messenger\Notifications;

use Doctoreto\Messenger\Values\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Doctoreto\Messenger\Channels\SmsChannel;
use Doctoreto\Messenger\Messages\SmsMessage;

class UserAppointmentCancelledByDoctor extends Notification implements ShouldQueue
{
    use Queueable;

    protected $doctorName;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        string $doctorName
    ) {
        $this->doctorName = $doctorName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return (new SmsMessage())
                ->content(
                    "دکترِتو: نوبت شما از دکتر " . $this->doctorName .
                    " ازجانب مطب ایشان لغو گردید. مبلغ پرداختی شما به حساب کاربریتان افزوده شد و میتوانید مجددا نوبت گیری نمایید."
                    . "%0a" . Constants::SUPPORT_NUMBER
                );
    }
}
