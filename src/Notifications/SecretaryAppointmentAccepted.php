<?php

namespace Doctoreto\Messenger\Notifications;

use Doctoreto\Messenger\Values\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Doctoreto\Messenger\Channels\SmsChannel;
use Doctoreto\Messenger\Messages\SmsMessage;

class SecretaryAppointmentAccepted extends Notification implements ShouldQueue
{
    use Queueable;

    protected $patientName;
    protected $patientMobile;
    protected $doctorName;
    protected $date;
    protected $hour;
    protected $trackingCode;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        string $patientName,
        string $patientMobile,
        string $doctorName,
        string $date,
        string $hour,
        string $trackingCode
    )
    {
        $this->patientName = $patientName;
        $this->patientMobile = $patientMobile;
        $this->doctorName = $doctorName;
        $this->date = $date;
        $this->hour = $hour;
        $this->trackingCode = $trackingCode;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return (new SmsMessage())
            ->header("دکترتو:")
            ->content(
                "ثبت نوبت " . $this->patientName .
                " برای روز " . $this->date .
                " ساعت " . $this->hour .
                " از دکتر " . $this->doctorName .
                "%0a" .
                " کد رهگیری " . $this->trackingCode .
                "%0a" .
                " شماره بیمار " . $this->patientMobile
                . "%0a" . Constants::SUPPORT_NUMBER
            );
    }
}
