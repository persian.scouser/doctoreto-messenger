<?php

namespace Doctoreto\Messenger\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Doctoreto\Messenger\Channels\OneSignalChannel;
use Doctoreto\Messenger\Messages\OneSignalMessage;

class CustomPushNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $content;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(string $header, string $content, string $url = null)
    {
        $this->header = $header;
        $this->content = $content;
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [OneSignalChannel::class];
    }

    public function toOneSignal($notifiable)
    {
        return (new OneSignalMessage($this->content))
                ->header($this->header)
                ->url($this->url);
    }
}
