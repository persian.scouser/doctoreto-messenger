<?php

namespace Doctoreto\Messenger\Notifications;

use Doctoreto\Messenger\Values\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Doctoreto\Messenger\Channels\SmsChannel;
use Doctoreto\Messenger\Messages\SmsMessage;

class UserAppointmentAccepted extends Notification implements ShouldQueue
{
    use Queueable;

    protected $doctorName;
    protected $day;
    protected $hour;
    protected $trackingCode;
    
    public $tries = 0;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        string $doctorName,
        string $day,
        string $hour,
        string $trackingCode
    ) {
        $this->doctorName = $doctorName;
        $this->day = $day;
        $this->hour = $hour;
        $this->trackingCode = $trackingCode;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return (new SmsMessage())
                ->header("دکترتو:")
                ->content(
                    "نوبت شما از دکتر " . $this->doctorName .
                    " برای روز" . $this->day .
                    " ساعت " . $this->hour .
                    " ثبت گردید. کد رهگیری:" . $this->trackingCode.
                    "زمان انتظار شما با توجه به روند کاری هر مطب غیر قابل پیش بینی میباشد. لطفا صبور باشید."
                    . "%0a" . Constants::SUPPORT_NUMBER
                );
    }
}
