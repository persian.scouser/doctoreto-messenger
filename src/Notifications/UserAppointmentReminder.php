<?php

namespace Doctoreto\Messenger\Notifications;

use Doctoreto\Messenger\Values\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Doctoreto\Messenger\Channels\SmsChannel;
use Doctoreto\Messenger\Messages\SmsMessage;

class UserAppointmentReminder extends Notification implements ShouldQueue
{
    use Queueable;

    protected $doctorName;
    protected $hour;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        string $doctorName,
        string $hour
    ) {
        $this->doctorName = $doctorName;
        $this->hour = $hour;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return (new SmsMessage())
                ->header("دکترتو:")
                ->content(
                    "یادآوری نوبت شما از دکتر " . $this->doctorName .
                    "برای فردا ساعت " . $this->hour
                    . "%0a" . Constants::SUPPORT_NUMBER
                );
    }
}
